#!/bin/bash

function checkEnv() {
  if [[ -z "$DB_HOST" ]]; then
    echo "DB_HOST is not set"
    exit 1
  fi
  if [[ -z "$DB_NAME" ]]; then
    echo "DB_NAME is not set"
    exit 1
  fi
  if [[ -z "$DB_USER" ]]; then
    echo "DB_USER is not set"
    exit 1
  fi
  if [[ -z "$DB_PASSWORD" ]]; then
    echo "DB_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$REDIS_HOST" ]]; then
    echo "REDIS_HOST is not set"
    exit 1
  fi
  if [[ -z "$REDIS_PORT" ]]; then
    echo "REDIS_PORT is not set"
    exit 1
  fi
  if [[ -z "$CACHE_TTL" ]]; then
    echo "CACHE_TTL is not set"
    exit 1
  fi
  if [[ -z "$MONGO_REPLICA_PRIMARY" ]]; then
    echo "MONGO_REPLICA_PRIMARY is not set"
    exit 1
  fi
  if [[ -z "$MONGO_REPLICA_SECONDARY_1" ]]; then
    echo "MONGO_REPLICA_SECONDARY_1 is not set"
    exit 1
  fi
  if [[ -z "$MONGO_REPLICA_SECONDARY_2" ]]; then
    echo "MONGO_REPLICA_SECONDARY_2 is not set"
    exit 1
  fi
  if [[ -z "$MONGO_REPLICA_PRIMARY_PORT" ]]; then
    echo "MONGO_REPLICA_PRIMARY_PORT is not set"
    exit 1
  fi
  if [[ -z "$MONGO_REPLICA_SECONDARY_1_PORT" ]]; then
    echo "MONGO_REPLICA_SECONDARY_1_PORT is not set"
    exit 1
  fi
  if [[ -z "$MONGO_REPLICA_SECONDARY_2_PORT" ]]; then
    echo "MONGO_REPLICA_SECONDARY_2_PORT is not set"
    exit 1
  fi
  if [[ -z "$READ_PREFERENCE" ]]; then
    echo "READ_PREFERENCE is not set"
    exit 1
  fi
  if [[ -z "$REPLICA_SET_NAME" ]]; then
    echo "REPLICA_SET_NAME is not set"
    exit 1
  fi
  if [[ -z "$NODE_ENV" ]]; then
    export NODE_ENV=production
  fi
}

function checkConnection() {
  echo "Connect MongoDB . . ."
  timeout 10 bash -c 'until printf "" 2>>/dev/null >>/dev/tcp/$0/$1; do sleep 1; done' $MONGO_REPLICA_PRIMARY $MONGO_REPLICA_PRIMARY_PORT
}

function configureServer() {
  if [ ! -f .env ]; then
    envsubst '${DB_HOST}
      ${DB_NAME}
      ${DB_USER}
      ${DB_PASSWORD}
      ${REDIS_HOST}
      ${REDIS_PORT}
      ${CACHE_TTL}
      ${NODE_ENV}
      ${MONGO_REPLICA_PRIMARY}
      ${MONGO_REPLICA_SECONDARY_1}
      ${MONGO_REPLICA_SECONDARY_2}
      ${MONGO_REPLICA_PRIMARY_PORT}
      ${MONGO_REPLICA_SECONDARY_1_PORT}
      ${MONGO_REPLICA_SECONDARY_2_PORT}
      ${READ_PREFERENCE}
      ${REPLICA_SET_NAME}' < docker/env.tmpl > .env
  fi
}
export -f configureServer

if [ "$1" = 'rollback' ]; then
  checkEnv
  checkConnection
  configureServer
  echo "Rollback migrations"
  # Uncomment the following line when the migration script is available
  # su arcapps -c "./node_modules/.bin/migrate down updateRoleScopeUuid -d mongodb://$MONGO_REPLICA_PRIMARY:$MONGO_REPLICA_PRIMARY_PORT/$DB_NAME"
fi

if [ "$1" = 'start' ]; then
  checkEnv
  checkConnection
  configureServer
  echo "Run migrations"
  # Uncomment the following line when the migration script is available
  # su arcapps -c "./node_modules/.bin/migrate up -d mongodb://$MONGO_REPLICA_PRIMARY:$MONGO_REPLICA_PRIMARY_PORT/$DB_NAME"
  su arcapps -c "node dist/main.js"
fi

exec runuser -u arcapps "$@"
