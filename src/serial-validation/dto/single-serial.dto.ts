import { ApiProperty } from '@nestjs/swagger';

export class SingleSerialDto {
  @ApiProperty({
    type: String,
    required: true,
    description: 'The serial number to validate.',
    example: '1234567890',
  })
  serial: string;
}
