import { ApiProperty } from '@nestjs/swagger';

export class SerialListDto {
  @ApiProperty({
    type: [String],
    required: true,
    description: 'The list of serial numbers to validate.',
    example: ['1234567890', '0987654321'],
  })
  serials: string[];
}
