import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { RedisCacheService } from 'src/common/redis/redis.service';
import { createApiResponse } from '../../common/constants/create-api.response';
import {
  FAIELD_RESPONSE,
  INTERNAL_SERVER_ERROR,
  SERIAL_FOUND,
  SERIAL_NOT_FOUND,
  SERIAL_PARTIALLY_FOUND,
  SUCCESS_RESPONSE,
} from '../../common/constants/messages';
import { SERIAL_NO_COLLECTION } from '../../common/constants/string';

@Injectable()
export class SerialValidationService {
  constructor(
    @InjectConnection() private readonly connection: Connection,
    private readonly redisService: RedisCacheService,
  ) {}

  async findSingleSerial(serial) {
    try {
      // try to find the serial in redis cache
      const cacheKey = this.redisService.generateKey('serial', serial);
      const redisData = await this.redisService.get(cacheKey);

      // if found in redis cache, return the data
      if (redisData) {
        return createApiResponse(
          HttpStatus.OK,
          SUCCESS_RESPONSE,
          SERIAL_FOUND,
          JSON.parse(redisData),
        );
      }

      // if not found in redis cache, find in mongodb
      const collection = this.connection.collection('serial_no');
      //
      const data = await collection
        .aggregate([
          { $match: { serial_no: serial } },
          {
            $lookup: {
              from: 'customer',
              localField: 'customer',
              foreignField: 'name',
              as: 'customer_details',
            },
          },
          { $unwind: '$customer_details' },
        ])
        .toArray();

      // if not found in mongodb, return not found
      if (data.length === 0) {
        return createApiResponse(
          HttpStatus.NOT_FOUND,
          FAIELD_RESPONSE,
          SERIAL_NOT_FOUND,
          null,
        );
      }

      // if found in mongodb, save to redis cache and return the data
      await this.redisService.setWithExpiry(
        cacheKey,
        JSON.stringify(data[0]),
        parseInt(process.env.CACHE_TTL) || 60,
      );

      // return the data
      return createApiResponse(
        HttpStatus.OK,
        SUCCESS_RESPONSE,
        SERIAL_FOUND,
        data[0],
      );
    } catch (error) {
      return createApiResponse(
        HttpStatus.INTERNAL_SERVER_ERROR,
        FAIELD_RESPONSE,
        INTERNAL_SERVER_ERROR,
      );
    }
  }

  async findSerialList(serials: string[]) {
    // try to find the serials in redis cache
    const key = this.redisService.generateSerialListKey('serials', serials);
    const redisData = await this.redisService.get(key);

    // if found in redis cache, return the data
    if (redisData) {
      return createApiResponse(
        HttpStatus.OK,
        SUCCESS_RESPONSE,
        SERIAL_FOUND,
        JSON.parse(redisData),
      );
    }

    // if not found in redis cache, find in mongodb
    const collection = this.connection.collection(SERIAL_NO_COLLECTION);
    const data = await collection
      .aggregate([
        { $match: { serial_no: { $in: serials } } },
        {
          $lookup: {
            from: 'customer',
            localField: 'customer',
            foreignField: 'name',
            as: 'customer_details',
          },
        },
        { $unwind: '$customer_details' },
      ])
      .toArray();

    // if not found in mongodb, return not found
    if (data.length === 0) {
      return createApiResponse(
        HttpStatus.NOT_FOUND,
        FAIELD_RESPONSE,
        SERIAL_NOT_FOUND,
        data,
      );
    }

    const foundSerials = data.map((item) => item.serial_no);
    if (foundSerials.length === serials.length) {
      // if found in mongodb, save to redis cache and return the data
      await this.redisService.setWithExpiry(
        key,
        JSON.stringify(data),
        parseInt(process.env.CACHE_TTL) || 60,
      );

      // return the data
      return createApiResponse(
        HttpStatus.OK,
        SUCCESS_RESPONSE,
        SERIAL_FOUND,
        data,
      );
    } else {
      // if found in mongodb, save to redis cache and return the data
      await this.redisService.setWithExpiry(
        key,
        JSON.stringify({
          found_serials: {
            total_serials: serials.length,
            founded_serials: foundSerials.length,
            serials: foundSerials,
          },
          data,
        }),
        parseInt(process.env.CACHE_TTL) || 60,
      );

      // return the data
      return createApiResponse(
        HttpStatus.OK,
        SUCCESS_RESPONSE,
        SERIAL_PARTIALLY_FOUND,
        {
          found_serials: {
            total_serials: serials.length,
            founded_serials: foundSerials.length,
            serials: foundSerials,
          },
          data,
        },
      );
    }
  }
}
