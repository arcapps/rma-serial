import { Test, TestingModule } from '@nestjs/testing';
import { SerialValidationService } from './serial-validation.service';

describe('SerialValidationService', () => {
  let service: SerialValidationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SerialValidationService,
        {
          provide: 'DatabaseConnection',
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<SerialValidationService>(SerialValidationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
