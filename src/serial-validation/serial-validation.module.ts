import { Module } from '@nestjs/common';
import { RedisModule } from 'src/common/redis/redis.module';
import { SerialValidationController } from './controller/serial-validation.controller';
import { SerialValidationService } from './service/serial-validation.service';

@Module({
  imports: [RedisModule],
  controllers: [SerialValidationController],
  providers: [SerialValidationService],
})
export class SerialValidationModule {}
