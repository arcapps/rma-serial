import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { SerialValidationService } from '../service/serial-validation.service';
import { SerialValidationController } from './serial-validation.controller';

describe('SerialValidationController', () => {
  let controller: SerialValidationController;
  let service: jest.Mocked<SerialValidationService>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SerialValidationController],
      providers: [
        {
          provide: SerialValidationService,
          useValue: {
            findSingleSerial: jest.fn(),
            findSerialList: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<SerialValidationController>(
      SerialValidationController,
    );
    service = module.get<SerialValidationService>(
      SerialValidationService,
    ) as jest.Mocked<SerialValidationService>;
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should find a single serial', async () => {
    const serial = 'ABC123';
    const response = {
      statusCode: HttpStatus.OK,
      response: 'success',
      message: 'Serial found',
      payload: { serial: 'ABC123', status: 'active' },
    };
    service.findSingleSerial.mockResolvedValue(response); // Using the mocked method
    await expect(controller.findSingleSerial(serial)).resolves.toEqual(
      response,
    );
    expect(service.findSingleSerial).toHaveBeenCalledWith(serial);
  });

  it('should find a list of serials', async () => {
    const serials = ['ABC123', 'XYZ789'];
    const response = {
      statusCode: HttpStatus.OK,
      response: 'success',
      message: 'Serials found',
      payload: [
        { serial: 'ABC123', status: 'active' },
        { serial: 'XYZ789', status: 'inactive' },
      ],
    };
    service.findSerialList.mockResolvedValue(response); // Using the mocked method
    await expect(controller.findSerialList(serials)).resolves.toEqual(response);
    expect(service.findSerialList).toHaveBeenCalledWith(serials);
  });
});
