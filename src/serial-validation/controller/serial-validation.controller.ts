import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SerialValidationService } from '../service/serial-validation.service';

@Controller('serial-validation')
@ApiTags('Serial Validation')
export class SerialValidationController {
  constructor(
    private readonly serialValidationService: SerialValidationService,
  ) {}

  @Get('single/:serial')
  async findSingleSerial(@Param('serial') serial: string) {
    return await this.serialValidationService.findSingleSerial(serial);
  }

  @Get('list')
  findSerialList(@Query('serials') serials: string[]) {
    return this.serialValidationService.findSerialList(serials);
  }
}
