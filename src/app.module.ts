import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ThrottlerModule } from '@nestjs/throttler';
import { getDefaultDbConnectionString } from './common/constants/mongo.connection';
import { SerialValidationModule } from './serial-validation/serial-validation.module';

@Module({
  imports: [
    MongooseModule.forRoot(getDefaultDbConnectionString()),
    ThrottlerModule.forRoot([
      {
        name: 'short',
        ttl: 1000,
        limit: 3,
      },
      {
        name: 'medium',
        ttl: 10000,
        limit: 20,
      },
      {
        name: 'long',
        ttl: 60000,
        limit: 100,
      },
    ]),
    SerialValidationModule,
  ],
  providers: [],
})
export class AppModule {}
