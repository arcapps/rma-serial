export const SUCCESS_RESPONSE = 'Success';
export const FAIELD_RESPONSE = 'Faield';
export const INTERNAL_SERVER_ERROR = 'Internal server error';
export const SERIAL_NOT_FOUND = 'Serial not found';
export const SERIAL_FOUND = 'Serial found';
export const SERIAL_PARTIALLY_FOUND = 'Serial partially found';
