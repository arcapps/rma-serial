// src/config/redis.config.ts
import {
  CACHE_TTL,
  ConfigService,
  REDIS_HOST,
  REDIS_PORT,
} from '../config/config.service';

const config = new ConfigService();

export function getDefaultRedisConfig() {
  return {
    store: require('cache-manager-redis-store'),
    host: config.get(REDIS_HOST),
    port: config.get(REDIS_PORT),
    ttl: config.get(CACHE_TTL),
    isGlobal: true,
    no_ready_check: true,
  };
}
