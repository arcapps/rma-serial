import { CacheModule } from '@nestjs/cache-manager';
import { Global, Module } from '@nestjs/common';
import { RedisCacheService } from './redis.service';

@Global()
@Module({
  imports: [CacheModule.register()],
  providers: [RedisCacheService],
  exports: [RedisCacheService],
})
export class RedisModule {}
