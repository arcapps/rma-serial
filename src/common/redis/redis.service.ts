import { Injectable } from '@nestjs/common';
import Redis from 'ioredis';

@Injectable()
export class RedisCacheService {
  private redis: Redis;

  constructor() {
    this.redis = new Redis({
      host: process.env.REDIS_HOST,
      port: parseInt(process.env.REDIS_PORT),
    });
  }

  async get(key: string): Promise<string> {
    return this.redis.get(key);
  }

  async set(key: string, value: string): Promise<void> {
    this.redis.set(key, value);
  }

  async del(key: string): Promise<void> {
    this.redis.del(key);
  }

  async setWithExpiry(
    key: string,
    value: string,
    expiry: number,
  ): Promise<void> {
    this.redis.set(key, value, 'EX', expiry);
  }

  async deleteKeysByPattern(pattern: string): Promise<void> {
    const keys = await this.redis.keys(pattern);
    if (keys?.length) {
      const pipeline = this.redis.pipeline();
      for (const key of keys) {
        pipeline.del(key);
      }
      await pipeline.exec();
    }

    return Promise.resolve();
  }

  generateKey(prefix: string, key: string): string {
    return `${prefix}:${key}`;
  }

  generateSerialListKey(prefix: string, keys: string[]): string {
    return `${prefix}:${keys.join(':')}`;
  }
}
