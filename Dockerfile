# Stage 1: Build
FROM node:latest AS build
# Copy app
COPY . /home/arcapps/rikoul-server
WORKDIR /home/arcapps/rikoul-server

# Install dependencies and build the project
RUN yarn install \
    && yarn build \
    && rm -fr node_modules \
    && yarn install --production

# Stage 2: Final Image
FROM node:slim

# Install packages
RUN apt-get update && apt-get install -y gettext-base && apt-get clean

# Setup docker-entrypoint
COPY docker/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

# Add non-root user
RUN useradd -ms /bin/bash arcapps
WORKDIR /home/arcapps/rikoul-server
COPY --from=build /home/arcapps/rikoul-server .
# Expose port
EXPOSE 3000

# Ensure entrypoint script is executable
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
